import csv
import sys

languages = ("fr", "en", "es")
data = {}

for l in languages:
    data[l] = []

reader = csv.DictReader(sys.stdin, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
for row in reader:
    if row["lan"] in languages:
        data[row["lan"]].append(row)

for language in data:
    with open(f"datasets/CFU_{language}.tsv", mode="w") as file:
        writer = csv.DictWriter(file, data[language][0].keys(), delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer.writeheader()
        for row in data[language]:
            if row["id"] not in ("comment_58807", "comment_78444"):
                writer.writerow(row)

    print(f"{language} completed")
