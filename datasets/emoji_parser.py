import emoji
import sys
import regex
import csv

emoji_list = []
with open("data-stats-with-comment.tsv") as data_file:
    data_parsed = csv.DictReader(data_file, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    for row in data_parsed:
        emoji_list += [c for c in regex.findall(r'\X', row["comment"]) if (emoji.is_emoji(c) and row["alignment"] == "Against")]
    
emoji_set = set(emoji_list)
print(*emoji_set, sep='\n')