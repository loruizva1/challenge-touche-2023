import sys
import csv
import nltk
from nltk.tokenize import word_tokenize, sent_tokenize
import itertools

language_corres = {
        'cs':'czech',
        'da':'danish',
        'nl':'dutch',
        'en':'english',
        'et':'estonian',
        'fi':'finnish',
        'fr':'french',
        'de':'german',
        'el':'greek',
        'it':'italian',
        'no':'norwegian',
        'pl':'polish',
        'pt':'portuguese',
        'ru':'russian',
        'sl':'slovene',
        'es':'spanish',
        'sv':'swedish',
        'tr':'turkish'
    }

def main():
    nltk.download("punkt")
    
    with open(f"CFS.tsv") as cfs, open(f"CFU.tsv") as cfu, open(f"CFE-D.tsv") as cfed:
        cfs_data = csv.DictReader(cfs, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        cfu_data = csv.DictReader(cfu, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        cfed_data = csv.DictReader(cfed, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        print("id\tid_prop\talignment\ttokens\tsentences\tTopic\tlan\tdataset")

        for row in itertools.chain(cfs_data, cfed_data, cfu_data):
            alignment = row["alignment"]
            if alignment != "None":
                dataset = "CFs"
            else:
                try:
                    if row["label"]:
                        dataset = "CFe-dev"
                        alignment = row["label"]
                except KeyError:
                    dataset = "CFu"

            comment_len = unit_len(row["comment"], row["lan"])
            tokens = comment_len["tokens"]
            sentences = comment_len["sentences"]

            print(f'{row["id"]}\t{row["id_prop"]}\t{alignment}\t{tokens}\t{sentences}\t{row["Topic"]}\t{row["lan"]}\t{dataset}')
    


def unit_len(x, lan):
    """
    get the number of tokens
    :param x: str
    :return: int with the tokens' number
    """
    if lan in language_corres:
        lan = language_corres[lan]
    else:
        lan = "english"

    freq = dict()
    freq["tokens"] = len(word_tokenize(x, language=lan))
    freq["sentences"] = len(sent_tokenize(x, language=lan))
    return freq


def rel_freq(x):
    """
    returns the relative frequency from a data
    :param x:
    :return: int with the relative frequency
    """
    freqs = [(value, x.count(value) / len(x)) for value in set(x)]
    return freqs


def average(lst):
    """
    :param lst: list with value
    :return: avarage from a list value
    """
    average_value = sum(lst) / len(lst)
    rounded_average = round(average_value, 2)
    return rounded_average


def all_freq():
    units = list(data["comment"])
    freq_list = []
    for i in range(0, len(units)):
        freq = unit_len(units[i])
        freq_list.append(freq)
    total_tok = sum_units(freq_list, "tokens")
    total_sentences = sum_units(freq_list, "sentences")
    return total_tok, total_sentences


def sum_units(lst, unit):
    units_list = []
    for freq in lst:
        for k, v in freq.items():
            if k == unit:
                units_list.append(v)
    unit_average = average(units_list)
    return (
        f"Nombre de {unit}:",
        sum(units_list),
        f"Nombre moyen de {unit}:",
        unit_average,
    )


def build_table(rows:list, columns:list):
    table = [{column:(None if column!=columns[0] else row) for column in columns} for row in rows]
    return table


def print_html(table):
    print("<table style='border-collapse: collapse;font-family: Geneva, sans-serif;'>")
    #Display columns
    print("<tr>")
    for c in table[0]:
        print("<th style='background-color: #54585d;color: #ffffff;font-weight: bold;font-size: 17px;border: 1px solid #54585d;padding: 15px;'>" + c + "</th>")
    print("</tr>")
    for row in table:
        print("<tr>")
        for val in row.values():
            print("<td style='padding: 15px;color: #636363;border: 1px solid #dddfe1;'>" + (f"{val:.2f}" if isinstance(val,float) else f"{val}") + "</td>")
        print("</tr>")

    print("</table>")


if __name__ == "__main__":
    main()