# Description des données en anglais (en)

### Description générale des données en anglais

Les commentaires en anglais sont répartis sur 3 jeu de données (désormais _datasets_), nous proposons un aperçu de la répartition des ces données dans le tableau ci-dessous :

|Dataset|Nombre de commentaires en anglais|Pourcentage par rapport au dataset|Pour (et %)|Contre (et %)|Neutre (et %)|
|---|---|---|---|---|---|
|CFs|3192|45.59%|2333 (73.09%)|859 (26.91%)|X|
|CFu|6635|50.22%|X|X|X|
|CFe-d|675|47.74%|351 (52.00%)|71 (10.52%)|253 (37.48%)|

La langue anglaise est donc la langue la plus utilisée dans toutes les données disponibles.

### Description détaillée du dataset CFs

Le dataset **CFs** contient **3192** commentaires rédigés en anglais, et répartis sur les 10 sujets disponibles de la façon suivante :

|Topic|Nombre de commentaires en anglais|Pourcentage par rapport au dataset|Pour (et %)|Contre (et %)|
|---|---|---|---|---|
|Migration|267|8.36%|153 (57.30%)|114 (42.70%)|
|GreenDeal|460|14.41%|349 (75.87%)|111 (24.13%)|
|Health|172|5.39%|148 (86.05%)|24 (13.95%)|
|Economy|335|10.49%|243 (72.54%)|92 (27.46%)|
|EUInTheWorld|302|9.46%|219 (72.52%)|83 (27.48%)|
|ValuesRights|348|10.90%|208 (59.77%)|140 (40.23%)|
|Digital|171|5.36%|137 (80.12%)|34 (19.88%)|
|Democracy|669|20.96%|471 (70.40%)|198 (29.60%)|
|Education|229|7.17%|215 (93.89%)|14 (6.11%)|
|OtherIdeas|239|7.49%|190 (79.50%)|49 (20.50%)|

Le dataset **CFs** contient des commentaires annotés par les utilisateurs et rédigés en anglais.

La langue anglaise est utilisée pour discuter de tous les sujets disponibles dans les données, malgré le fait que les utilisateurs n'ont souvent pas l'anglais comme langue maternelle. En effet, la plupart des commentaires ont été rédigés après le Brexit, ce qui signifie que les utilisateurs sont des ressortissants de pays ayant une langue officielle autre que l'anglais.

Celle-ci est tout de même fortement présente car il s'agit de la langue d'échange internationale, ainsi que la langue dans laquelle sont traduites les propositions.